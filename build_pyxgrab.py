import cffi

ffibuilder = cffi.FFI()

c_header = """
void getScreenGeometry(uint32_t *, uint32_t *, uint32_t *);
void getScreen(const int32_t, const int32_t, const uint32_t, const uint32_t, uint8_t *);
"""

c_source = """
#include <stdio.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

typedef enum { true=1, false=0 } bool;

bool getScreenGeometry(uint32_t *width, uint32_t *height, uint32_t *depth)
{
    bool status = false;
    Window root;
    int x, y;
    unsigned int border_width;

    Display *display = XOpenDisplay(NULL);
    int screen_num = DefaultScreen(display);

    /* Get geometry information about root window */
    if (XGetGeometry(display, RootWindow(display, screen_num), &root,
        &x, &y, width, height, &border_width, depth) == False)
        {
            fprintf(stderr, "can\'t get root window geometry");
            status = true;
        }
    return status;
}

void getScreen(const int32_t xx, const int32_t yy, const uint32_t W, const uint32_t H, /*out*/ uint8_t * data)
{
   Display *display = XOpenDisplay(NULL);
   Window root = DefaultRootWindow(display);

   XImage *image = XGetImage(display, root, xx,yy, W,H, AllPlanes, ZPixmap);

   unsigned long red_mask   = image->red_mask;
   unsigned long green_mask = image->green_mask;
   unsigned long blue_mask  = image->blue_mask;
   unsigned int x, y;
   unsigned int ii = 0;
   for (y = 0; y < H; y++) {
       for (x = 0; x < W; x++) {
         unsigned long pixel = XGetPixel(image,x,y);
         unsigned char blue  = (pixel & blue_mask);
         unsigned char green = (pixel & green_mask) >> 8;
         unsigned char red   = (pixel & red_mask) >> 16;

         data[ii + 2] = blue;
         data[ii + 1] = green;
         data[ii + 0] = red;
         ii += 3;
      }
   }
   XDestroyImage(image);
   XDestroyWindow(display, root);
   XCloseDisplay(display);
}
"""

ffibuilder.set_source("pyxgrab._pyxgrab", c_source,
                      libraries=['X11'],
                      extra_compile_args=['-O3'])

ffibuilder.cdef(c_header)


if __name__ == "__main__":
    ffibuilder.compile(verbose=True)

