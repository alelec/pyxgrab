import sys
from os import path
from setuptools import setup

# Default modules required to run setuptools
setup_requires = ['pytest-runner', "setuptools_scm"]

build_require = ['cffi>=1.0.0']
tests_require = ['pytest', 'pytest-cov', 'xvfbwrapper']

# If building the package, we also need cffi
if any(('build' in arg or 'bdist' in arg for arg in sys.argv)):
    setup_requires.extend(build_require)

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'README.rst'), 'r') as f:
    long_description = f.read()

setup(
    name="pyxgrab",
    use_scm_version=True,
    packages=["pyxgrab"],
    description='X Windows screenshot grabber',
    long_description=long_description,
    url='https://gitlab.com/alelec/pyxgrab',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    cffi_modules=["build_pyxgrab.py:ffibuilder"],
    install_requires=['cfficloak', 'numpy'],
    setup_requires=setup_requires,
    tests_require=tests_require,
    extras_require={'test': tests_require,
                    'build': build_require},
)