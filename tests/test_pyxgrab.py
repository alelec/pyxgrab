import pyxgrab

import pytest
import numpy as np
from xvfbwrapper import Xvfb

@pytest.mark.parametrize("width,height", [
    (640, 480),
    (1024, 768),
    (1280, 720),
])
def test_pyxgrab(width, height):
    with Xvfb(width=width, height=height) as xvfb:
        image = pyxgrab.grab_screen()
        assert isinstance(image, np.ndarray)
        assert image.shape[0] == height
        assert image.shape[1] == width

def test_pyxgrab_coord():
    with Xvfb(width=1024, height=768) as xvfb:
        image = pyxgrab.grab_screen(x1=10, y1=100, x2=30, y2=200)
        assert isinstance(image, np.ndarray)
        assert image.shape[0] == 100
        assert image.shape[1] == 20


