pyxgrab
=======

Python fast screenshot grabber for X windows.

This is a simple small module to directly grab either the entire screen or a portion of the screen using libx XGetImage() functionality.

Usage::

    import pyxgrab

    # Grab entire screen, returned as numpy ndarray
    image = pyxgrab.grab_screen()

    # Grab rect from screen, again returned as ndarray
    image = pyxgrab.grab_screen(x1=10, y1=100, x2=30, y2=200)
