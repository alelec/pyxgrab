from __future__ import absolute_import

import cfficloak
import numpy as np
from . import _pyxgrab

__pyxgrab = cfficloak.wrapall(_pyxgrab.ffi, _pyxgrab.lib)

@cfficloak.function_skeleton(__pyxgrab, noret=True, outargs=(0,1,2))
def getScreenGeometry():
    """
    C function to grab geometry of default screen via xlib
    :return: (width, height, depth)
    """

@cfficloak.function_skeleton(__pyxgrab, noret=True)
def getScreen(x, y, W, H, data):
    """
    C function to grab screenshot via xlib
    :param int x: starting x coordinate
    :param int y: starting y coordinate
    :param int W: width in pixels
    :param int H: height in pixels
    :param buffer data: pre-allocated buffer equal to the W x H x 3 (for RGB data)
    :return: None
    """

def grab_screen(x1=None, y1=None, x2=None, y2=None):

    if None in (x1, x2, y1, y2):
        x1 = y1 = 0
        w, h, depth = getScreenGeometry()
    else:
        w, h = x2-x1, y2-y1
        depth = 24

    buff = np.ndarray((h, w, depth//8), dtype=np.uint8)
    getScreen(x1, y1, w, h, cfficloak.nparrayptr(buff))

    return buff
